# EMT - EDeA Merge Tool

This folder contains the software responsible for merging multiple KiCad Projects into one. It uses [Realthunder's kicad_parser library](https://github.com/realthunder/kicad_parser) as the foundation for handling the pcbnew files.

## How To Use

TODO

### Install Requirements

### Run in the Command Line

### Run as a Webservice in LXD

## How It Works

A KiCad (5) Project is a directory, containing exactly one `.kicad_pcb` PCB layout file, and one or more `.sch` circuit schematic files. The schematic is in eeschema's own text format, and the layout is pcbnew's s-expression format.

### PCB Layout

Since the KiCad's Python bindings (`kicad-python`) does not support enough of pcbnew's functionality, it is necessary to parse the PCB layout, do the necessary transformations, and then from the sum of the parts generate a new project. The main tasks are:

* Parse the s-expression into nested struct (object).
* Extract Metadata: Number of layers (copper, component), bounding box, track widths, isolation distance, via properties. This is data is also necessary for the Web Portal.
* Create list of nets. Rename all nets, apply a prefix to each, to avoid aliasing.
* Rename all component references, the same way we do with the schematic.
* Move everything by (X,Y) units. This means all coordinates need transformation.
* Append content to the output project.
* Write the internal representation as pcbnew s-expression into the new `.kicad_pcb` file.

### Schematic Diagram

Thankfully, eeschema has support for nested sheets. We can use this functionality to add the Modules as subcircuits. What needs to be done:

* Copy the schematic into the target project directory. Rename it in a way that it avoids filename collisions. Check before copying for existing file, do not overwrite.
* Rename nets the same way as we did with the PCB layout.
* Rename component references to avoid collisions. Similar to how annotations work in eeschema, we can add a digit in front, transforming 3-digit identifiers to 4 or 5-digit identifiers. This limits maximum component count to 1000 per Module.
* In the target project's main schematic file (the one that has the same file name as the project folder), the schematic file is added as a subcircuit.
* Parse the source schematic and find the external-connecting nets. Right now this means the "Hierarchical Labels" as used in eeschema. Then add the subcircuit ports to its symbol in the output project's main schematic. Place the hierarchical subsheet in a way that they do not overlap. (Fill up in an X-Y grid left-to-right, top-to-bottom.) Considering the complexitiy, for now, wiring these together is left for the end user. Since the PCB nets match the schematic's nets, a logical association is maintained. This makes it possible to work on, and change the generated projects just as if they were hand-made.

### Running Standalone

EMT can be used as a CLI tool. It needs a root directory where the Modules (which are KiCad Project directories) are stored, and one or more input Module name, and an output project name.

### Running as Webservice (Security Considerations)

For this tool to be used as a server-side public utility, precautions are necessary. To mitigate potential bugs, all user input must be considered as possibly malicious. The tool can run in container or VM, with no write access to anything except its output directory. After each run, the output directory is packed into an archive and sent back to the main application server. The output directory is then wiped.

All extracted metadata supplied by EMT to the Portal must be sanitized, as we might need to deal with hand-crafted malicious KiCad files. All uploaded Modules must be sanitized against HTML or Javascript injection (XSS) before adding it to the repository. In case of any irregularities (execution time exceeding limit, ouput file size exceeding limit, parse errors, invalid extraced metadata), the input file and request logs must be saved along with user information, and a notification to a website admin be sent.

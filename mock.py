#!/usr/bin/env python
# %%

import math
from emt import kicadpcb

# %%

class Point:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y
    
    def rotate(self, angle):
        self.x = self.x * math.cos(angle) + self.y * math.sin(angle)
        return self
    
    def coords(self):
        return self.x, self.y

class Rectangle:
    @staticmethod
    def fromPad(pad: kicadpcb.KicadPCB_pad):
        r = Rectangle()
        r.x = pad.at[0]
        r.y = pad.at[1]
        if len(pad.at) > 2:
            angle = pad.at[2]
        else:
            angle = 0
        r.w = math.fabs(pad.size[0] * math.cos(angle) + pad.size[1] * math.sin(angle))
        r.h = math.fabs(pad.size[1] * math.cos(angle) + pad.size[0] * math.sin(angle))
        return r

    def __init__(self):
        self.x = 0
        self.y = 0
        self.w = 0
        self.h = 0

    def fromCoords(self, x1=0, y1=0, x2=0, y2=0):
        self.x = (x1+x2) / 2.0
        self.y = (y1+y2) / 2.0
        self.w = math.fabs(x1-x2)
        self.h = math.fabs(y1-y2)
        self.y1, self.y2 = y1, y2     

    def coords(self):
        '''
        Returns x1, y1, x2, y2 where x1 <= x2 and y1 <= y2
        '''
        return self.x - self.w/2, self.y - self.h/2, self.x + self.w/2, self.y + self.h/2

    def extend(self, rect):
        '''
        Helper function to get the common bounding box.
        Returns a rectangle which covers both.
        '''
        c1 = rect.coords()
        c2 = self.coords()
        c3 = [0] * 4
        for i in range(2):
            c3[i] = min(c1[i], c2[i])
            c3[i+2] = max(c1[i+2], c2[i+2])
        r = Rectangle()
        r.fromCoords(*c3)
        return r

class Pad:
    def __init__(self, pad: kicadpcb.KicadPCB_pad):
        self.p = pad
        self.rect = Rectangle.fromPad(pad)

    def boundingBox(self) -> Rectangle:
        return self.rect

class KiFootprint:
    def __init__(self, module: kicadpcb.KicadPCB_module):
        self.m = module
        self.pads = list()
        if len(module.pad) > 0:
            for p in module.pad:
                self.pads.append(Pad(p))

    def movexy(self, x, y):
        self.m.at[0] += x
        self.m.at[1] += y
    
    def boundingBox(self) -> Rectangle:
        ''' 
        Gives you the bounding rectangle in global coordinates
        '''
        if len(self.pads) > 0:
            bb = self.pads[0].boundingBox()
            for p in self.pads:
                bb.extend(p.boundingBox())
        else:
            bb = Rectangle()
        bb.x += self.m.at[0]
        bb.y += self.m.at[1]
        # We don't handle module rotation yet!!
        return bb

class kipcb_merge(object):
    def __init__(self):
        self.min_x = float("inf")
        self.max_x = -self.min_x
        self.min_y = float("inf")
        self.max_y = -self.min_y
    
    def update_bb(self, x, y):
        self.max_x = max(self.max_x, x)
        self.max_y = max(self.max_y, y)
        self.min_x = min(self.min_x, x)
        self.min_y = min(self.min_y, y)
    
    def update_bb_pts(self, pts):
        for xy in pts.xy:
            self.update_bb(xy[0], xy[1])

# %%
pcb = kicadpcb.KicadPCB.load('tests/assets/5vpol.kicad_pcb')
pcb_orig = kicadpcb.KicadPCB.load('tests/assets/5vpol.kicad_pcb')

'''
merge = kipcb_merge()

for m in pcb.module:
    merge.update_bb(m.at[0], m.at[1])

for s in pcb.segment:
    merge.update_bb(s.start[0], s.start[1])
    merge.update_bb(s.end[0], s.end[1])

for v in pcb.via:
    merge.update_bb(v.at[0], v.at[1])

for d in pcb.dimension:
    merge.update_bb(d.gr_text.at[0], d.gr_text.at[1])
    merge.update_bb_pts(d.feature1.pts)
    merge.update_bb_pts(d.feature2.pts)
    merge.update_bb_pts(d.crossbar.pts)
    merge.update_bb_pts(d.arrow1a.pts)
    merge.update_bb_pts(d.arrow2a.pts)
    merge.update_bb_pts(d.arrow1b.pts)
    merge.update_bb_pts(d.arrow2b.pts)

for z in pcb.zone:
    for poly in z.polygon:
        merge.update_bb_pts(poly.pts)
    for poly in z.filled_polygon:
        merge.update_bb_pts(poly.pts)

for gr in pcb.gr_text:
    merge.update_bb(gr.at[0], gr.at[1])

for gr in pcb.gr_circle:
    merge.update_bb(gr.center[0], gr.center[1])
    merge.update_bb(gr.end[0], gr.end[1])

for gr in pcb.gr_arc:
    merge.update_bb(gr.start[0], gr.start[1])
    merge.update_bb(gr.end[0], gr.end[1])

for gr in pcb.gr_line:
    merge.update_bb(gr.start[0], gr.start[1])
    merge.update_bb(gr.end[0], gr.end[1])

print(F"max x: {merge.max_x}, max y: {merge.max_y}, min x: {merge.min_x}, min y: {merge.min_y}")
'''

# %%
def show_info(m: kicadpcb.KicadPCB_module):
    ref = m.fp_text[0][1]
    bb = m._boundingBox
    if bb.valid:
        print(("Comp {} at [{:03.2f} {:03.2f}], "
            +"\n\tbounding box width {:03.3f}, height {:03.3f}, "
            +"\n\tbb center at [{:03.2f} {:03.2f}]\n").format(
            ref, *(m.at[0:2]), bb.width, bb.height, *(bb.center)
        ))
    else:
        print("Comp {} at [{:03.2f} {:03.2f}] got no valid bounding box".format(
            ref, *(m.at[0:2])
        ))

led_d2 = KiFootprint(pcb.module[2])
ic_u1 = KiFootprint(pcb.module[0])

ld2bb = pcb.module[2]._boundingBox
print(ld2bb)

print(pcb.getBoundingBox())
# %%

for m in pcb.module:
    show_info(m)

# %%

# Testing the waters
ic1 = pcb.module[0]
j2  = pcb.module[6]
show_info(ic1)
show_info(j2)

bb = ic1._boundingBox
bb.envelop(j2._boundingBox.corners)
print(bb)

# %%
print("Moving center into 0,0")
pcbb = pcb.getBoundingBox()
x, y = pcbb.center
pcb.movexy(-x, -y)

for m in pcb.module:
    show_info(m)

# %%
print("Drawing a rectangle around everythinig")
pcb.drawBoundingBox()

bb = pcb.getBoundingBox()
x, y = bb.center
y -= bb.height/2 + 1
textlocation = x,y
text = "This is an EDeA Module with size ({:.1f}x{:.1f})".format(bb.width, bb.height)
pcb.drawText(textlocation, text)

# %%
print("Merging PCB with its umondified self")
pcb.add_pcb(pcb_orig)

# %%
print("Exporting modified pcb")
pcb.export("tmp/merged.kicad_pcb")
print("done.")

# %%
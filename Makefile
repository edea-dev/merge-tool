venv-create:
	python3 -m venv venv

venv-activate:
	source venv/bin/activate

init:
	pip install -r requirements.txt

test:
	py.test tests

.PHONY: venv-activate init test


#!/usr/bin/env python

from kicad_pcb import *
from sexp_parser import *
import sys
import argparse
import logging

parser = argparse.ArgumentParser()
parser.add_argument("filename",nargs='?')
parser.add_argument("-l", "--log", dest="logLevel", 
    choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'], 
    help="Set the logging level")
parser.add_argument("-o", "--output", help="output filename")
args = parser.parse_args()    
logging.basicConfig(level=args.logLevel,
        format="%(filename)s:%(lineno)s: %(levelname)s - %(message)s")

pcb = KicadPCB.load('5vpol.kicad_pcb' if args.filename is None else args.filename)

# check for error
for e in pcb.getError():
    print('Error: {}'.format(e))

"""
Things to move:

module <- kicad speech for component footprint
    - at <- X,Y,rot coordinates of the module, in GCS
dimension
    - feature{1,2}
        - pts
    - arrow{1a,1b,2a,2b}
        - pts
    - crossbar
        - pts
gr_circle
    - center
    - at <- X,Y in GCS (rotation doesn't make sense)
gr_text
    - at X,Y,rot in GCS
gr_arc
    - start X,Y
    - end X,Y
gr_line
    - start X,Y
    - end X,Y
gr_poly
    - pts
        - X,Y
segment
    - start
    - end
via
    - at
zone
    - polygon
        - array of xy
    - filled_polygon
        - array of xy

"""

def move_pts(pts, x, y):
    """
    move an array of pts by x and y
    """
    for p in pts:
        p += x
        p[2] += y




print('root values: ')
for k in pcb:
    print('\t{}: {}'.format(k,pcb[k]))

print('\nversion: {}'.format(pcb.version))

for k in pcb.layers:
    print('\t{}: {}'.format(k,pcb.layers[k]))

print('\nmodule[0] keys: {}'.format(pcb.module[0]))

# export it again
if args.output:
    pcb.export(sys.stdout if args.output=='-' else args.output)

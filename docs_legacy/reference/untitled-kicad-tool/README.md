# Untitled KiCad Merge Tool

## Status: PREALPHA, work in progress
What works: generating new projects and schematic files, including the selected modules as subschematics.

Untitled KiCad Merge Tool as a pcbnew plugin is in progress.

### Do not forget edge cases
- accidental aliasing of recursive subcircuits: default behavior is to rename file and rewrite `F1` references
- intentional aliasing of recursive subcircuits: add option for fingerprinting and deduplication

## Description
The _Untitled KiCad Merge Tool_ (UKMT) is a KiCad circuit submodule stitcher.

This tool can be used to create a new project from existing KiCad projects. Their schematics will be copied into the target and included in the main schematic as hierarchical sheets, while merging their PCB layout and retraining footprint associations.

This program should not exist and the functionality it provides should be done by KiCad. Since Eeschema 5 is not scriptable, no one seemed to make this thing??? Anyways, I hope KiCad 6 will make this unnecessary. Until then, it's very useful!

## How to use it / best practices
Run `./ukmt.py --output "project_name" --module "module1" "module2" [...]` where

* `project_name` is the target project you want to merge into
* `module1`, `module2` are root directories of KiCad project folders. You can use the same module multiple times, in which case UKMT will insert them multiple times. Accidental filename collisions are detected and avoided automatically; if `module1` contains `a.sch` as a subschematic, and `module2` also contains `a.sch` as a subschematic, the target project will use a single `a.sch` in case those source files are identical in content. Otherwise, UKMT will rename the second file and update the references.
* `--overwrite` is a safety switch you need to use if you want UKMT to modify an existing project.

UKMT parses the top-level schematic in each source module. It generates a list of hierarchical labels, and all the subcircutis (if any). Then it creates (or updates) the top-level schematic in the target project, adding the module as subcircuits in the top first free position right of the existing wires in that sheet. The hierarchical labels are used to generate ports of these subsheets; inputs top left, outputs top right, everything else bottom left. You will need to connect the modules on your own.

Please note that all the matching global labels of the modules will be connected together; if you use a `+3V3`, `GND`, `AGND`, etc. power flag in multiple modules, those will be connected in the resulting project. This might be undesirable if you want to keep separate power domains. For that you will need to use hierarchical labels instead of global labels, and wire the modules together in the top-level schematic. You can also use the local net label tool.

A few example modules are included in this project. You can try out UKMT by running for example `./ukmt.py --output test --module 3v3ldo microcontroller can can`

*Pull requests, bug reports, ideas are welcome*

Tell your story of using the Untitled KiCad Merge Tool

## Begging for Money
If this program saved you time, please consider [donating](https://ko-fi.com/zoe1337). Thank you.

## Future Plans
I'd love to have a website and community where circuit snippets can be shared, discussed, and bigger projects created from modules. Building an open-source / open-hardware community library, which would enable us, the community, to avoid reinventing the wheel, do more, achieve more, and have more and better quality custom open hardware.

## License
Copyright 2020, Fully Automated OÜ

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE  
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,  
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN  
THE SOFTWARE.

The source code is released under the 
[GNU General Public License v2.0](http://www.gnu.org/licenses/gpl-2.0.html)
- for other licensing options, please contact us at `untitled-kicad-tool AT automated DOT ee` .

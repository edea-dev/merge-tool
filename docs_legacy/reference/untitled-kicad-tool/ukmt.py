#!/usr/bin/env python

# Copyright (c) 2020 by Fully Automated OÜ
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import argparse
import os
import shutil
import datetime
import time
import kicad
import re
import string

p = None

class Project:
    # Provide spacing between hierarchical subsheet symbols, ports
    eeschema_gap = 200

    # Provide spacing between PCB layout modules
    pcbnew_gap = 350000

    # initial target coordinates
    schematic_topright = [0, 0]
    pcb_topright = [0, 0]

    # list of subschematics extracted from the modules
    additional_schematic_files = []

    def __init__(self, output_project):
        _, self.project_name = os.path.split(output_project)
        self.project_output_dir = os.path.abspath(output_project)
        self.schematic_filepath = os.path.join(self.project_output_dir, output_project + ".sch")
        self.project_filepath = os.path.join(self.project_output_dir, output_project + ".pro")
        self.pcb_filepath = os.path.join(self.project_output_dir, output_project + ".kicad_pcb")
        self.schematic = Schematic()
        self.overwrite_enabled = False

    def create_output_folder(self):
        os.makedirs(self.project_output_dir)

        minimumProject = [
            'update=22/05/2015 07:44:53',
            'version=1',
            'last_client=kicad',
            '[general]',
            'version=1',
        ]
        with open(self.project_filepath, "wt") as f:
            for line in minimumProject:
                print(line, file=f)

class Pcb:
    class Net:
        def __init__(self, parsed_sexpr):
            self.net_id = parsed_sexpr[1][0]
            if type(parsed_sexpr[2]) is tuple:
                self.net_name = parsed_sexpr[2][0]
            else:
                self.net_name = parsed_sexpr[2]

        def net_to_subsheet(self, uuid):
            if not self.net_name == "":
                self.net_name = '/' + uuid + self.net_name

        def __repr__(self):
            return f"net {self.net_id}: \'{self.net_name}\'"

        def to_parsed_sexpr(self):
            return [('net',), (f'{self.net_id}',), (f'{self.net_name}',)]

        def getId(self):
            return int(self.net_id)
        
        def setId(self, new_id):
            self.net_id = f"{new_id}"

    class Pts:
        def __init__(self, parsed_sexpr):
            self.coords = []
            if type(parsed_sexpr) is list:
                for entry in parsed_sexpr:
                    if type(entry) is list:
                        x = float(entry[1][0])
                        y = float(entry[2][0])
                        coord = [x, y]
                        self.coords.append(coord)
            # print(self.coords)

        def move(self, xy):
            for point in self.coords:
                point[0], point[1] = point[0] + xy[0], point[1] + xy[1]

        def getsexpr(self):
            retval = [('pts',)]
            for point in self.coords:
                l = [('xy',), (str(point[0]),), (str(point[1]),)]
                retval.append(l)
            return retval


    # TODO put parser in a separate module or something
    atom_end = set('()') | set(string.whitespace)
    def _parse(self, sexp):
        stack, i, length = [[]], 0, len(sexp)
        while i < length:
            c = sexp[i]

            reading = type(stack[-1])
            if reading == list:
                if   c == '(': stack.append([])
                elif c == ')': 
                    stack[-2].append(stack.pop())
                    if stack[-1][0] == ('quote',): stack[-2].append(stack.pop())
                elif c == '"': stack.append('')
                elif c == "'": stack.append([('quote',)])
                elif c in string.whitespace: pass
                else: stack.append((c,))
            elif reading == str:
                if   c == '"': 
                    stack[-2].append(stack.pop())
                    if stack[-1][0] == ('quote',): stack[-2].append(stack.pop())
                elif c == '\\': 
                    i += 1
                    stack[-1] += sexp[i]
                else: stack[-1] += c
            elif reading == tuple:
                if c in Pcb.atom_end:
                    atom = stack.pop()
                    stack[-1].append(atom)
                    if stack[-1][0] == ('quote',): stack[-2].append(stack.pop())
                    continue
                else: stack[-1] = ((stack[-1][0] + c),)
            i += 1
        return stack.pop()

    def _print_sexp(self, exp):
        out = ''
        if type(exp) == type([]):
            out += '(' + ' '.join(self._print_sexp(x) for x in exp) + ')'
        elif type(exp) == type(''):
            if re.search(r'[\s()]', exp):
                out += '"%s"' % repr(exp)[1:-1]#.replace('"', '\"')
            else:
                out += '""'
        else:
            out += '%s' % exp
        return out[0]
    
    def loadf(self):
        with open(self.filepath, 'rt') as inpf:
            self.sexp = inpf.read()
        self.document = self._parse(self.sexp)
        self._parseNets()
        self._parseZones()

    def getSymbol(self, symbol):
        retval = []
        for node in self.document[0]:
            if type(node) is list:
                label = node[0]
                if type(label) is tuple:
                    if label[0] == symbol:
                        retval.append(node)
        return retval

    def _parseZones(self):
        self.zones = self.getSymbol("zone")
        for z in self.zones:
            for subexpr in z:
                if type(subexpr) is list:
                    if type(subexpr[0]) is tuple:
                        label = subexpr[0][0]
                        if label == "polygon" or label == "filled_polygon":
                            points = Pcb.Pts(subexpr[1])
                            points.move([10,-5000])
                            subexpr[1] = points.getsexpr()
        print(self.document)

    def _parseNets(self):
        self.nets = []
        for node in self.document[0]:
            if type(node) is list:
                label = node[0]
                if type(label) is tuple:
                    if label[0] == "net":
                        self.nets.append(Pcb.Net(node))
        print(f"Parsed nets: {self.nets}")

    def __init__(self, filepath):
        self.filepath = filepath
        if filepath is not None:
            self.loadf()
    
    def writef(self):
        with open(self.filepath + "-test.kicad_pcb", 'wt') as outpf:
            outpf.write(self._print_sexp(self.document))

class Module:
    def __init__(self, module_spec, schema_xy):
        self.module_spec = module_spec
        self.module_source_dir = self.find_dir()
        _, self.module_name = os.path.split(self.module_source_dir)
        self.schematic_filename = f"{self.module_name}.sch"
        self.module_schematic_file = os.path.join(self.module_source_dir, self.schematic_filename)
        self.module_pcb_file = os.path.join(self.module_source_dir, f"{self.module_name}.kicad_pcb")
        self.subckt = Subcircuit(self.module_schematic_file, schema_xy)
        self.pcbGeom = [0, 0, 0, 0]
        self.pcb = Pcb(self.module_pcb_file)

    def find_dir(self):
        # TODO add some library root path and search for there if the module is not found in the local dir
        return os.path.abspath(self.module_spec)

    def copy_data(self, source_file, destination_file):
        # TODO add overwrite protection
        src = source_file
        if self.module_source_dir:
            src = os.path.join(self.module_source_dir, source_file)
        dst = os.path.join(p.project_output_dir, destination_file)
        shutil.copy2(src, dst)


class SchematicComponent:
    def __init__(self):
        self.timestamp = ''
        self.ar = {}
        self.symbol = ''

class Schematic:
    """
    This is more of a wrapper than a real parser
    values are only touched if necessary, string lists are kept.
    There are some asserts to protect against doing mad things.
    """
    header = [
        'EESchema Schematic File Version 4',
        'EELAYER 30 0',
        'EELAYER END']
    descr = [
        '$Descr A4 11693 8268',
        'encoding utf-8',
        'Sheet 1 1', #.format(str(numberOfSheets),
        'Title ""',
        'Date ""',
        'Rev ""',
        'Comp ""',
        'Comment1 "Fully Automated Modular Circuit Generator"',
        'Comment2 ""',
        'Comment3 ""',
        'Comment4 ""',
        '$EndDescr']
    body = [],
    subcircuits = [], # this is the only list that's not just text
    footer = "$EndSCHEMATC"
    components = [],

    def __init__(self):
        self.body = []
        self.subcircuits = []
        self.setDatestr()
        self.target_loc = [700, 800]
        self.curr_comp = SchematicComponent()

    def setDatestr(self):
        datestr = datetime.datetime.now().replace(microsecond=0).isoformat()
        assert self.descr[4].startswith('Date')        
        self.descr[4] = f'Date "{datestr}"'

    def getNumberOfSheets(self):
        NoS = 1+len(self.subcircuits)
        assert self.descr[2].startswith("Sheet")
        self.descr[2] = f"Sheet 1 {NoS}"
        return NoS

    def setComments(self, comments):
        n = 1
        for cmnt in comments:
            assert self.descr[6+n].startswith('Comment')
            self.descr[6+n] = f'Comment{n} "{cmnt}"'
            n = n+1

    def getSubckts(self):
        output = []
        for subckt in self.subcircuits:
            output = output + subckt.render()
        return output

    def render(self):
        self.getNumberOfSheets()
        output = self.header
        output = output + self.descr
        output = output + self.body
        output = output + self.getSubckts()
        output.append(self.footer)
        return output

    def parse(self, source_file_path):
        state = 0
        idx = 0
        parseError = True
        self.subcircuits = []
        self.body = []
        with open(source_file_path, 'rt') as sf:
            for line in sf:
                line = line.rstrip()
                if state == 0: # look for header
                    print(line)
                    assert line == self.header[0]
                    state = 1
                    idx = 1

                elif state == 1: # header
                    if line.startswith('$Descr'):
                        state = 2
                        idx = 1
                        self.descr[0] = line
                    else: 
                        self.header[idx] = line
                        idx = idx+1

                elif state == 2: # descr
                    if line.startswith('$EndDescr'):
                        self.descr[idx] = line
                        state = 3
                        idx = 0
                    else:
                        self.descr[idx] = line
                        idx = idx+1

                elif state == 3: # body
                    if line == self.footer:
                        parseError = False
                    elif line == '$Comp': 
                        state = 4
                    else:
                        self.body.append(line)
                        idx = idx+1
                
                elif state == 4: # component
                    self.body.append(line)
                    idx += 1
                    if line == '$EndComp':
                        state = 3
                        idx += 1
        assert not parseError

    def _parseComponentLine(self, line):
        if line.startswith('L'):
            self.curr_comp.symbol = line.split(' ')[1]
            self.curr_comp.designator = line.split(' ')[2]
        elif line.startswith('U'):
            self.curr_comp.timestamp = line.split(' ')[3]
        elif line.startswith('AR'):
            pass

    
    def findBoundingBox(self):
        thisLineContainsCoordinates = False
        [x, y] = self.target_loc
        for line in self.body:
            if thisLineContainsCoordinates:
                thisLineContainsCoordinates = False
                values = line.split()
                x = max([x, int(values[0]), int(values[2])])
            elif line == "Wire Wire Line":
                thisLineContainsCoordinates = True
        self.target_loc = [p.eeschema_gap * 2 + x, y]

class Subcircuit:
    class Port:
        def __init__(self, portstring, name):
            self.name = name
            parameters = portstring.split()
            mode = parameters[6][0:1]
            # tri-state is 3State in the subcircuit but T on the port definition
            if mode == "3":
                mode = "T"
            self.mode = mode
            self.side = "L"
            if self.isOutput():
                self.side = "R"
            self.x0, self.y0 = 0, 0

        def __repr__(self):
            # F2 "IO" B L 500 600 50 <- example
            # 50 at the end is some kind of magic, no idea!
            return f'F{self.N} "{self.name}" {self.mode} {self.side} {self.x0} {self.y0} 50'

        def isInput(self):
            return ("I" == self.mode)

        def isOutput(self):
            return ("O" == self.mode)

    seq = 0
    def __init__(self, subckt_filepath, target_loc):
        self.name = ""
        self.ports = []
        self.portlist_namesonly = []
        self.x0, self.y0 = target_loc[0], target_loc[1]
        self.w, self.h = 1900, 1900
        self.filepath = subckt_filepath
        self.basepath, self.filename = os.path.split(subckt_filepath)
        unixtime_int = int(time.time())
        serial = unixtime_int+Subcircuit.seq
        self.uuid = f"{serial:08x}".upper()
        """okay this is literally a 32-bit unix timestamp as hex, BUT
            we need to make this unique otherwise its useless"""
        self.generate_ports()
        self.h = max([500, (len(self.ports)+2)*p.eeschema_gap])
        Subcircuit.seq = 1+Subcircuit.seq

    def getNextX0Y0(self):
        return [self.x0, self.y0 + self.h + 2*p.eeschema_gap]

    def render(self):
        output = [
            '$Sheet',
            f'S {self.x0}  {self.y0}  {self.w} {self.h}', 
            f'U {self.uuid}', 
            f'F0 "{self.name}" 50', 
            f'F1 "{self.filename}" 50']
        for port in self.ports:
            output.append(repr(port))
        output.append('$EndSheet')
        return output
    
    def generate_ports(self):
        subckt_path = self.filepath
        N=2
        inputports, outputports, otherports = 0, 0, 0
        with open(subckt_path, "rt") as f:
            for current_line in f:
                if current_line.startswith("Text HLabel"):
                    """This is a hierarchical port"""
                    signal_name = f.readline().rstrip()
                    if signal_name not in set(self.portlist_namesonly):
                        self.portlist_namesonly.append(signal_name)
                        port = Subcircuit.Port(current_line, signal_name)
                        port.N = N
                        N = N+1
                        if port.isInput():
                            inputports = inputports + 1
                        elif port.isOutput():
                            outputports = outputports + 1
                        else:
                            otherports = otherports + 1
                        self.ports.append(port)
                    else:
                        print(f"Warning: \"{signal_name}\" is reused on sheet {self.filename}, ignoring multiples.")
                elif current_line.startswith("Title "):
                    """Sheet title. Let's use this as subcircuit name!"""
                    self.name = current_line.split('"')[1]
                elif current_line.startswith("F1 "):
                    """Uh! Recursive subcircuits!"""
                    p.additional_schematic_files.append()
        self.h = min([p.eeschema_gap * (2 + max([(inputports + otherports), outputports])), 3*p.eeschema_gap])
        [leftY, rightY] = [self.y0 + p.eeschema_gap]*2
        leftbottomY = self.y0 + self.h - p.eeschema_gap
        for port in self.ports:
            port.x0 = self.x0
            if port.isInput():
                port.y0 = leftY
                leftY = leftY + p.eeschema_gap
            elif port.isOutput():
                port.x0 = self.x0 + self.w
                port.y0 = rightY
                rightY = rightY + p.eeschema_gap
            else:
                port.y0 = leftbottomY
                leftbottomY = leftbottomY - p.eeschema_gap

def main():
    parser = argparse.ArgumentParser(
        description="Insert subcircuit(s) into a (new) KiCad project including its PCB layout.",
        epilog="Lorem ipsum"
    )
    parser.add_argument(
        "--overwrite",
        help='Enable overwriting of existing project',
        action='store_true',
        dest='overwrite',
        default=False
    )
    parser.add_argument(
        "--output",
        help='Output project name',
        type=str,
        required=True
    )
    parser.add_argument(
        "--module",
        #type=argparse.FileType("d"), # this isn't a thing :/
        type=str,
        help="Directory of the module you want to insert",
        required=True,
        nargs="+"
    )
    parser.add_argument(
        "--parse",
        type=str,
        default=""
    )

    args = parser.parse_args()
    output_project = args.output
    modules = args.module

    global p
    p = Project(output_project)
    p.overwrite_enabled = args.overwrite

    # TODO make sure no eeschema is running with the target open - see psutil 
    # https://thispointer.com/python-check-if-a-process-is-running-by-name-and-find-its-process-id-pid/

    # prepare output directory
    p.create_output_folder()
    if args.parse != "":
        p.schematic.parse(os.path.abspath(args.parse))
        p.schematic.findBoundingBox()

    # instantiate modules
    [x0, y0] = p.schematic.target_loc
    for module_spec in modules:
        module = Module(module_spec, [x0, y0])
        [x0, y0] = module.subckt.getNextX0Y0()
        p.schematic.subcircuits.append(module.subckt)
        module.copy_data(module.schematic_filename, module.schematic_filename)
        module.pcb.writef()

    with open(p.schematic_filepath, "wt") as out_sch:
        for line in p.schematic.render():
            print(line, file=out_sch)
    
    return

if __name__ == '__main__':
    main()

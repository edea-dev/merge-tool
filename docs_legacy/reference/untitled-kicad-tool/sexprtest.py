#!/usr/bin/env python
import pprint
import re
from string import whitespace

atom_end = set('()') | set(string.whitespace)

def parse(sexp):
    stack, i, length = [[]], 0, len(sexp)
    while i < length:
        c = sexp[i]

        reading = type(stack[-1])
        if reading == list:
            if   c == '(': stack.append([])
            elif c == ')': 
                stack[-2].append(stack.pop())
                if stack[-1][0] == ('quote',): stack[-2].append(stack.pop())
            elif c == '"': stack.append('')
            elif c == "'": stack.append([('quote',)])
            elif c in whitespace: pass
            else: stack.append((c,))
        elif reading == str:
            if   c == '"': 
                stack[-2].append(stack.pop())
                if stack[-1][0] == ('quote',): stack[-2].append(stack.pop())
            elif c == '\\': 
                i += 1
                stack[-1] += sexp[i]
            else: stack[-1] += c
        elif reading == tuple:
            if c in atom_end:
                atom = stack.pop()
                stack[-1].append(atom)
                if stack[-1][0] == ('quote',): stack[-2].append(stack.pop())
                continue
            else: stack[-1] = ((stack[-1][0] + c),)
        i += 1
    return stack.pop()

# TODO: pretty print the result
def print_sexp(exp):
    out = ''
    if type(exp) == type([]):
        out += '(' + ' '.join(print_sexp(x) for x in exp) + ')'
    elif type(exp) == type(''):
        if re.search(r'[\s()]', exp):
            out += '"%s"' % repr(exp)[1:-1]#.replace('"', '\"')
        else:
            out += '""'
    else:
        out += '%s' % exp
    return out
 
 
if __name__ == '__main__':
    with open("testdata/3v3ldo/3v3ldo.kicad_pcb", 'rt') as inpf:
        sexp = inpf.read()
 
    diyparsed = parse(sexp)
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(diyparsed)

    with open('testdata/3v3ldo/sexprtest.kicad_pcb', 'wt') as outpf:
        outpf.write(print_sexp(diyparsed[0]))

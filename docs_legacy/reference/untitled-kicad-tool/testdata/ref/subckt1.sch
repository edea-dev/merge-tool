EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	3050 2850 3450 2850
Wire Wire Line
	3450 2850 3450 3250
Wire Wire Line
	3450 3250 4500 3250
Text Label 3050 2850 0    50   ~ 0
SIGNAL
Text HLabel 4500 3250 2    50   BiDi ~ 0
BIDIR
Text HLabel 4500 2450 2    50   UnSpc ~ 0
PASSIVE
Wire Wire Line
	4500 2450 3800 2450
Text Label 3800 2450 0    50   ~ 0
INTERNAL_SIGNAL2
Wire Wire Line
	4000 2800 4500 2800
Text Label 4000 2800 0    50   ~ 0
input
Text Label 4000 2950 0    50   ~ 0
GLOBSIG
Wire Wire Line
	4000 2950 4500 2950
Text HLabel 4500 2950 2    50   3State ~ 0
tristate
Wire Wire Line
	4000 2650 4500 2650
Text Label 4000 2650 0    50   ~ 0
output
Text HLabel 4500 2650 2    50   Output ~ 0
output
Text HLabel 4500 2800 2    50   Input ~ 0
input
$EndSCHEMATC

EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 0    0    1850 1850
U 5E234A27
F0 "Subcircuit One" 50
F1 "subckt1.sch" 50
F2 "IO" B L 0   100 50 
F3 "PASSIVE" U L 0   200 50 
F4 "tristate" T L 0   300 50 
F5 "output" O L 0   400 50 
F6 "BIDIR" B L 0   600 50 
F7 "input" I L 0   500 50 
$EndSheet
$Sheet
S 0    1850 1850 1650
U 5E239939
F0 "Subcircuit 2" 50
F1 "subckt1.sch" 50
$EndSheet
$Sheet
S 0    3700 1850 1400
U 5E239A75
F0 "Subcircuit 3" 50
F1 "subcircuit3.sch" 50
$EndSheet
Wire Wire Line
	3500 3600 2900 3600
$EndSCHEMATC

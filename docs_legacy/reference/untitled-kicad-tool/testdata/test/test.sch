EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 2
Title ""
Date "2020-01-23T07:26:34"
Rev ""
Comp ""
Comment1 "Fully Automated Modular Circuit Generator"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 700  800  1900 1000
U 5E293C9A
F0 "A 3.3-v LDO as a demo" 50
F1 "3v3ldo.sch" 50
F2 "GND" U L 700 1200 50
F3 "VIN" I L 700 1000 50
F4 "VOUT" O R 2600 1000 50
$EndSheet
$EndSCHEMATC

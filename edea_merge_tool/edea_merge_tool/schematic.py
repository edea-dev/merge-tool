#!/usr/bin/env python3

"""
Copyright (c) 2021 Fully Automated OÜ
SPDX-License-Identifier: EUPL-1.2
"""

import argparse
from kicadpcb import KicadPCB
import os
import shutil
import datetime
import time

p = None


class Pcb:
    def __init__(self, file) -> None:
        self.pcb = KicadPCB.load(file)


class Module:
    def __init__(self, module_spec, schema_xy, eeschema_gap):
        self.module_spec = module_spec
        self.module_source_dir = self.find_dir()
        _, self.module_name = os.path.split(self.module_source_dir)
        self.schematic_filename = f"{self.module_name}.sch"
        self.module_schematic_file = os.path.join(
            self.module_source_dir, self.schematic_filename)
        self.module_pcb_file = os.path.join(
            self.module_source_dir, f"{self.module_name}.kicad_pcb")
        self.subckt = Subcircuit(self.module_schematic_file,
                                 schema_xy, eeschema_gap)
        self.pcbGeom = [0, 0, 0, 0]
        self.pcb = Pcb(self.module_pcb_file)

    def find_dir(self):
        # TODO add some library root path and search for there if the module is not found in the local dir
        return os.path.abspath(self.module_spec)

    def copy_data(self, project_output_dir, source_file, destination_file):
        # TODO add overwrite protection
        src = source_file
        if self.module_source_dir:
            src = os.path.join(self.module_source_dir, source_file)
        dst = os.path.join(project_output_dir, destination_file)
        shutil.copy2(src, dst)


class Schematic:
    """This is more of a wrapper than a real parser
    values are only touched if necessary, string lists are kept.
    There are some asserts to protect against doing mad things."""
    header = [
        'EESchema Schematic File Version 4',
        'EELAYER 30 0',
        'EELAYER END']
    descr = [
        '$Descr A4 11693 8268',
        'encoding utf-8',
        'Sheet 1 1',  # .format(str(numberOfSheets),
        'Title ""',
        'Date ""',
        'Rev ""',
        'Comp ""',
        'Comment1 "Fully Automated Modular Circuit Generator"',
        'Comment2 ""',
        'Comment3 ""',
        'Comment4 ""',
        '$EndDescr']
    body = [],
    subcircuits = [],  # this is the only list that's not just text
    footer = "$EndSCHEMATC"

    def __init__(self):
        self.body = []
        self.subcircuits = []
        self.setDatestr()
        self.target_loc = [700, 800]

    def setDatestr(self):
        datestr = datetime.datetime.now().replace(microsecond=0).isoformat()
        assert self.descr[4].startswith('Date')
        self.descr[4] = f'Date "{datestr}"'

    def getNumberOfSheets(self):
        NoS = 1 + len(self.subcircuits)
        assert self.descr[2].startswith("Sheet")
        self.descr[2] = f"Sheet 1 {NoS}"
        return NoS

    def setComments(self, comments):
        n = 1
        for cmnt in comments:
            assert self.descr[6 + n].startswith('Comment')
            self.descr[6 + n] = f'Comment{n} "{cmnt}"'
            n = n + 1

    def getSubckts(self):
        output = []
        for subckt in self.subcircuits:
            output = output + subckt.render()
        return output

    def render(self):
        self.getNumberOfSheets()
        output = self.header
        output = output + self.descr
        output = output + self.body
        output = output + self.getSubckts()
        output.append(self.footer)
        return output

    def parse(self, source_file_path):
        state = 0
        idx = 0
        parseError = True
        self.subcircuits = []
        self.body = []
        with open(source_file_path, 'rt') as sf:
            for line in sf:
                line = line.rstrip()
                if state == 0:  # look for header
                    assert line == self.header[0]
                    state = 1
                    idx = 1

                elif state == 1:  # header
                    if line.startswith('$Descr'):
                        state = 2
                        idx = 1
                        self.descr[0] = line
                    else:
                        self.header[idx] = line
                        idx = idx + 1

                elif state == 2:  # descr
                    if line.startswith('$EndDescr'):
                        self.descr[idx] = line
                        state = 3
                        idx = 0
                    else:
                        self.descr[idx] = line
                        idx = idx + 1

                elif state == 3:  # body
                    if line == self.footer:
                        parseError = False
                    else:
                        self.body.append(line)
                        idx = idx + 1
        assert not parseError

    def findBoundingBox(self):
        thisLineContainsCoordinates = False
        [x, y] = self.target_loc
        for line in self.body:
            if thisLineContainsCoordinates:
                thisLineContainsCoordinates = False
                values = line.split()
                x = max([x, int(values[0]), int(values[2])])
            elif line == "Wire Wire Line":
                thisLineContainsCoordinates = True
        self.target_loc = [p.eeschema_gap * 2 + x, y]


class Subcircuit:
    class Port:
        def __init__(self, portstring, name):
            self.name = name
            parameters = portstring.split()
            mode = parameters[6][0:1]
            # tri-state is 3State in the subcircuit but T on the port definition
            if mode == "3":
                mode = "T"
            self.mode = mode
            self.side = "L"
            if self.isOutput():
                self.side = "R"
            self.x0, self.y0 = 0, 0

        def __repr__(self):
            # F2 "IO" B L 500 600 50 <- example
            # 50 at the end is some kind of magic, no idea!
            return f'F{self.N} "{self.name}" {self.mode} {self.side} {self.x0} {self.y0} 50'

        def isInput(self):
            return ("I" == self.mode)

        def isOutput(self):
            return ("O" == self.mode)

    seq = 0

    def __init__(self, subckt_filepath, target_loc, eeschema_gap):
        self.name = ""
        self.eeschema_gap = eeschema_gap
        self.ports = []
        self.portlist_namesonly = []
        self.x0, self.y0 = target_loc[0], target_loc[1]
        self.w, self.h = 1900, 1900
        self.filepath = subckt_filepath
        self.basepath, self.filename = os.path.split(subckt_filepath)
        unixtime_int = int(time.time())
        serial = unixtime_int + Subcircuit.seq
        self.uuid = f"{serial:08x}".upper()
        """okay this is literally a 32-bit unix timestamp as hex, BUT
            we need to make this unique otherwise its useless"""
        self.generate_ports()
        self.h = max([500, (len(self.ports) + 2) * self.eeschema_gap])
        Subcircuit.seq = 1 + Subcircuit.seq

    def getNextX0Y0(self):
        return [self.x0, self.y0 + self.h + 2 * self.eeschema_gap]

    def render(self):
        output = [
            '$Sheet',
            f'S {self.x0}  {self.y0}  {self.w} {self.h}',
            f'U {self.uuid}',
            f'F0 "{self.filename}" 50',  # TODO: set to self.name
            f'F1 "{self.filename}" 50']
        for port in self.ports:
            output.append(repr(port))
        output.append('$EndSheet')
        return output

    def generate_ports(self):
        subckt_path = self.filepath
        N = 2
        inputports, outputports, otherports = 0, 0, 0
        with open(subckt_path, "rt") as f:
            for current_line in f:
                if current_line.startswith("Text HLabel"):
                    """This is a hierarchical port"""
                    signal_name = f.readline().rstrip()
                    if signal_name not in set(self.portlist_namesonly):
                        self.portlist_namesonly.append(signal_name)
                        port = Subcircuit.Port(current_line, signal_name)
                        port.N = N
                        N = N + 1
                        if port.isInput():
                            inputports = inputports + 1
                        elif port.isOutput():
                            outputports = outputports + 1
                        else:
                            otherports = otherports + 1
                        self.ports.append(port)
                    else:
                        print(
                            f"Warning: \"{signal_name}\" is reused on sheet {self.filename}, ignoring multiples.")
                elif current_line.startswith("Title "):
                    """Sheet title. Let's use this as subcircuit name!"""
                    self.name = current_line.split('"')[1]
                elif current_line.startswith("F1 "):
                    """Uh! Recursive subcircuits!"""
                    p.additional_schematic_files.append()
        self.h = min(
            [self.eeschema_gap * (2 + max([(inputports + otherports), outputports])), 3 * self.eeschema_gap])
        [leftY, rightY] = [self.y0 + self.eeschema_gap] * 2
        leftbottomY = self.y0 + self.h - self.eeschema_gap
        for port in self.ports:
            port.x0 = self.x0
            if port.isInput():
                port.y0 = leftY
                leftY = leftY + self.eeschema_gap
            elif port.isOutput():
                port.x0 = self.x0 + self.w
                port.y0 = rightY
                rightY = rightY + self.eeschema_gap
            else:
                port.y0 = leftbottomY
                leftbottomY = leftbottomY - self.eeschema_gap

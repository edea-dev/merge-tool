import math
import numpy as np

"""
Initial code: https://github.com/ulikoehler/UliEngineering/blob/master/UliEngineering/Math/Coordinates.py
    by Uli Köhler, Apache-2.0

Changes: Copyright (c) 2021 Fully Automated OÜ
SPDX-License-Identifier: Apache-2.0
"""


class BoundingBox:

    def __init__(self, points):
        """
        Compute the upright 2D bounding box for a set of
        2D coordinates in a (n,2) numpy array.
        You can access the bbox using the
        (minx, maxx, miny, maxy) members.
        """
        self.reset()
        self.envelop(points)

    @staticmethod
    def rot(xy, angle):
        xi, yi = xy
        si, co = math.sin(angle), math.cos(angle)
        xo = xi * co + yi * si
        yo = yi * co + xi * si
        return [xo, yo]

    def envelop(self, points):
        """
        Envelop the existing bounding box with new points
        This might need optimization, we're doing some unnecessary
        math for the sake of programmatic simplicity
        """
        if points is None or len(points) == 0:
            return
        if len(points.shape) != 2 or points.shape[1] != 2:
            raise ValueError(
                "Points must be a (n,2), array but it has shape {}".format(
                    points.shape)
            )
        if self._valid:
            extended = np.concatenate((points, self.corners))
        else:
            extended = points
        self._valid = True
        self.minx, self.miny = np.min(extended, axis=0)
        self.maxx, self.maxy = np.max(extended, axis=0)

    def translate(self, coords):
        """
        move the bounding box by [x y]
        this is used for coordinate system transformation
        """
        if self._valid:
            self.minx += coords[0]
            self.maxx += coords[0]
            self.miny += coords[1]
            self.maxy += coords[1]

    def reset(self):
        self.minx, self.miny = float("inf") * np.array([1, 1], dtype=np.float64)
        self.maxx, self.maxy = float("inf") * np.array([-1, -1], dtype=np.float64)
        self._valid = False

    def rotate(self, angle):
        """
        rotate the box around the origin. angle is in degrees
        """
        if self._valid:
            c = self.corners
            rotated = np.zeros((4, 2), dtype=np.float64)
            angle = angle / math.tau
            si, co = math.sin(angle), math.cos(angle)
            for i in range(len(c)):
                xo = c[i][0] * co + c[i][1] * si
                yo = c[i][1] * co + c[i][0] * si
                rotated[i] = [xo, yo]
            self.reset()
            self.envelop(rotated)

    @property
    def corners(self):
        """
        If the bounding box is empty, this returns None.
        Returns all four corners of this rectangle in a [4][2] float64 array
        """
        if self._valid:
            return np.array(
                [
                    [self.minx, self.miny],
                    [self.minx, self.maxy],
                    [self.maxx, self.maxy],
                    [self.maxx, self.miny],
                ],
                dtype=np.float64,
            )
        else:
            return None  # np.array([[]])

    @property
    def valid(self):
        return self._valid

    @property
    def width(self):
        """X-axis extent of the bounding box"""
        if self._valid:
            return self.maxx - self.minx
        else:
            return 0

    @property
    def height(self):
        """Y-axis extent of the bounding box"""
        if self._valid:
            return self.maxy - self.miny
        else:
            return 0

    @property
    def area(self):
        """width * height"""
        return self.width * self.height

    @property
    def center(self):
        """(x,y) center point of the bounding box"""
        if self._valid:
            return (self.minx + self.width / 2, self.miny + self.height / 2)
        else:
            return False  # I don't want to return 0,0

    def __repr__(self):
        if self._valid:
            return (
                "BoundingBox([{:03.2f}x, {:03.2f}y] -> [{:03.2f}X, {:03.2f}Y])".format(
                    self.minx, self.miny, self.maxx, self.maxy
                )
            )
        else:
            return "BoundingBox empty"

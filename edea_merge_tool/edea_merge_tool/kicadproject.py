"""[summary]
KiCAD Project File Abstraction

Copyright (c) 2021 Fully Automated OÜ
SPDX-License-Identifier: EUPL-1.2
"""

import os
from schematic import Schematic
from kicadpcb import KicadPCB


class Project:
    # Provide spacing between hierarchical subsheet symbols, ports
    eeschema_gap = 200

    # Provide spacing between PCB layout modules
    pcbnew_gap = 350000

    # initial target coordinates
    schematic_topright = [0, 0]
    pcb_topright = [0, 0]

    # list of subschematics extracted from the modules
    additional_schematic_files = []

    pcb = KicadPCB.empty()

    def __init__(self, output_project):
        _, self.project_name = os.path.split(output_project)
        self.project_output_dir = os.path.abspath(output_project)
        self.schematic_filepath = os.path.join(
            self.project_output_dir, self.project_name + ".sch")
        self.project_filepath = os.path.join(
            self.project_output_dir, self.project_name + ".pro")
        self.pcb_filepath = os.path.join(
            self.project_output_dir, self.project_name + ".kicad_pcb")
        self.schematic = Schematic()
        self.overwrite_enabled = False

    def create_output_folder(self):
        os.makedirs(self.project_output_dir)

        minimumProject = [
            'update=22/05/2015 07:44:53',
            'version=1',
            'last_client=kicad',
            '[general]',
            'version=1',
        ]
        with open(self.project_filepath, "w") as f:
            for line in minimumProject:
                print(line, file=f)
